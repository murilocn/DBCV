#!/usr/bin/env python
# coding: utf-8

# In[6]:


import numpy as np
import pandas as pd
from scipy.spatial import distance_matrix
from scipy.sparse.csgraph import minimum_spanning_tree
from scipy.sparse import csr_matrix
from sklearn import datasets
from functools import reduce
from sklearn.manifold import MDS
import matplotlib.pyplot as plt
import os


# In[7]:


def sendTelegramMsg(msg):
    cmd = "curl -X POST -H \"Content-Type: application/json\" -d '{\"chat_id\": \"-1001234760323\", \"text\": \""+msg+"\", \"disable_notification\": true}'  \"https://api.telegram.org/bot1254765904:AAG0HQNeFNxrrfpyyREGgP8mFX_DQ-dKUBM/sendMessage\""
    print(cmd)
    os.system(cmd)


# In[8]:


def sendTelegramPhoto(path):
    cmd = 'curl -F "chat_id=-1001234760323" -F "photo=@'+path+'" "https://api.telegram.org/bot1254765904:AAG0HQNeFNxrrfpyyREGgP8mFX_DQ-dKUBM/sendphoto"'
    print(cmd)
    os.system(cmd)


# In[9]:


def transform(smdist, mpts=2):
    cored = [a[mpts-1][0] for a in smdist]
    max_r = max(cored)
    woself = [a[1:] for a in smdist]
    dmreach = [[(max(item[0], cored[item[1]], cored[i]), item[1]) for item in a] for i,a in enumerate(woself)]
    adj_matrix = [[0 for _ in range(len(smdist))] for _ in range(len(smdist))]
    for i, row in enumerate(dmreach):
        for col in row:
            adj_matrix[i][col[1]] = col[0]
    return csr_matrix(adj_matrix), max_r


# In[10]:


def getMstList(m):
    mst = minimum_spanning_tree(m)
    return sorted(list(set([(col, (min(i,j), max(i,j))) for i,row in enumerate(mst.toarray()) for j,col in enumerate(row) if col != 0])), reverse=True)


# In[11]:


def mstByMpts(smdist, mpts=2):
    transformed, max_r = transform(smdist, mpts)
    return getMstList(transformed), max_r


# In[12]:


def compareRange(mst1, mst2):
    e1, m1 = mst1
    e2, m2 = mst2
    mm = max(m1, m2)
    i = 0
    while e1[i][0] > mm and e2[i][0] > mm:
        if e1[i] != e2[i]:
            return False
        i += 1
    return True


# In[13]:


def isNaldisHyphotesisValid(compmatrix):
    # reduce(AND, flat map)
    return reduce(lambda a,b: a and b, [elem for row in compmatrix for elem in row])


# In[1]:


def checkIfHyphotesisIsValidForDataset(dataset, n_samples, n_features, seed):
    figname = f"teste.png"
    plt.figure(figsize=(16,9))
    if n_features > 2:
        sendTelegramMsg("This is just a 2D representation of an "+n_features+"D dataset")
        enc = MDS(n_components=2)
        X_transformed = enc.fit_transform(dataset)
        plt.scatter(X_transformed[:,0],X_transformed[:,1])
    else:
        plt.scatter(dataset[:,0],dataset[:,1])
    plt.savefig(figname)
    sendTelegramMsg(f"Checking if Naldi hypothesis is valid for {n_samples} points with {n_features} features...")
    sendTelegramPhoto(figname)
    mdist = distance_matrix(dataset, dataset)
    smdist = [sorted([(num, i) for i, num in enumerate(a)]) for a in mdist]
    msts = []
    for i in range(2,101):
        msts.append(mstByMpts(smdist, i))
    compmatrix = [[compareRange(a,b) for b in msts] for a in msts]    
    result = isNaldisHyphotesisValid(compmatrix)
    if result:
        sendTelegramMsg("Nice, his hypothesis is valid! =D")
    else:
        sendTelegramMsg("Nope =(")


# In[83]:


h = [1000, 10000, 100000]
dim = [2,3,5,10,25,50,100]
for d in dim:
    for i in h:
        if d == 2:
            data = datasets.make_circles(n_samples=i, random_state=1337, noise=0.1, factor=0.1)[0]
            checkIfHyphotesisIsValidForDataset(data, i, d, 1337)
        if d == 3:
            data = datasets.make_swiss_roll(n_samples=i, noise=0.7, random_state=1337)[0]
            checkIfHyphotesisIsValidForDataset(data, i, d, 1337)
        data = datasets.make_blobs(n_samples=i, n_features=d, centers=3, random_state=1337)[0]
        checkIfHyphotesisIsValidForDataset(data, i, d, 1337)
        data = np.random.rand(i,d)
        checkIfHyphotesisIsValidForDataset(data, i, d, 1337)

