"""
Implimentation of Density-Based Clustering Validation "DBCV"

Citation:
Moulavi, Davoud, et al. "Density-based clustering validation."
Proceedings of the 2014 SIAM International Conference on Data Mining.
Society for Industrial and Applied Mathematics, 2014.
"""

import numpy as np
from scipy.spatial.distance import cdist
from scipy.sparse.csgraph import minimum_spanning_tree


def DBCV(dataset, labels, dist_function='euclidean'):
    """
    Density Based clustering validation

    Args:
        X (np.ndarray): ndarray with dimensions [n_samples, n_features]
            data to check validity of clustering
        labels (np.array): clustering assignments for data X
        dist_dunction (func): function to determine distance between objects
            func args must be [np.array, np.array] where each array is a point

    Returns: cluster_validity (float)
        score in range[-1, 1] indicating validity of clustering assignments
    """
    
    labels = np.array(labels)
    clusters = set(labels) 
    
    #treating singleton clusters as noise
    for i in clusters:
        cluster  = np.where(labels == i)[0]
        if len(cluster) == 1:
            labels[cluster] = -1 
            clusters = clusters - {i} 
        
    if -1 in clusters:

        #checking if partition has ate least 2 clusters, return 0 otherwise
        if len(clusters - {-1}) < 2:
            return 0 
        
        #removing outliers    
        outliers = np.where(labels == -1)[0]
        dataset = np.delete(dataset,outliers,0)
        labels = np.delete(labels,outliers,0)
        clusters = clusters - {-1} 
        
    else:
        
        #checking if partition has ate least 2 clusters, return 0 otherwise
        if len(clusters) < 2:
            return 0 
        
        outliers = []

        
    #calculating basic statistics
    n_samples, n_features = np.shape(dataset)
    core_distances = np.zeros(n_samples)
    cluster_size = np.zeros(len(clusters))
    cluster_size = cluster_size.astype(int)
    dist = cdist(dataset, dataset, metric=dist_function)**2
    intra_edges = np.zeros(len(clusters))
    inter_edges = np.ones(len(clusters)) * np.inf

    #calculating core distances and intra_cluster distances
    aux = 0 
    for cluster in clusters:
        
        #finding clustes        
        objcl = np.where(labels == cluster)[0]
        cluster_size[aux] = len(objcl)
        
        #finding cores distances for cluster 
        for i in objcl:
            distance_vector = dist[i][objcl]
            distance_vector = distance_vector[distance_vector != 0]
            numerator = ((1/distance_vector)**n_features).sum()
            core_distances[i] = (1/(numerator / (cluster_size[aux] - 1))) ** (1/n_features)            
            
        #building MRD graph for cluster
        graph = np.zeros((cluster_size[aux],cluster_size[aux]))  
        for i in range(cluster_size[aux]): 
            for j in range(i+1,cluster_size[aux]): 
                graph[i,j] = np.max([core_distances[objcl[i]], core_distances[objcl[j]], dist[objcl[i]][objcl[j]]])   
                graph[j,i] = graph[i,j]
                
        #find MST of cluster and minimum intra_edge       
        mst = minimum_spanning_tree(graph).toarray()  
        intra_edges[aux] = np.max(mst[np.nonzero(mst)]) 
                     
        aux = aux +1
        
    #finding mininum inter-edges
    aux = 0 
    for cluster in clusters:   
        
        #finding clustes        
        objcl = np.where(labels == cluster)[0]
        others = np.where(labels != cluster)[0]
        
        for i in objcl:
            for j in others:
                temp = np.max([core_distances[i], core_distances[j], dist[i][j]])
                if temp < inter_edges[aux]:
                    inter_edges[aux] = temp
                    
        aux = aux +1 

    valid = 0 
    #calculating index
    for i in range(len(clusters)):
        dbcvcl = (inter_edges[i] - intra_edges[i])/ max(inter_edges[i],intra_edges[i])
        valid = valid + (dbcvcl * cluster_size[i])

    valid = valid / (len(labels) + len(outliers))
    
    return valid






